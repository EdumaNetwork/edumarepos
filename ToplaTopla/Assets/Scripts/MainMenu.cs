﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelData
{
    public LevelData(string levelName)
    {
        string data = PlayerPrefs.GetString(levelName);
        if (data == "")
            return;
        string[] allData = data.Split('&');
        BestTime = float.Parse(allData[0]);
        SilverTime = float.Parse(allData[1]);
        GoldTime = float.Parse(allData[2]);
      //  Debug.Log(data);
    }
    public float BestTime { get; set; }
    public float GoldTime { get; set; }
    public float SilverTime { get; set; }
}
public class MainMenu : MonoBehaviour
{
    private const float CAMERA_TRANSITION_SPEED = 3.0f;
    public GameObject lvlButtonPrefab;
    public GameObject lvlButtonContainer;

    public GameObject shopButtonPrefab;
    public GameObject shopButtonContainer;

    private Transform cameraTransform;
    private Transform cameraDesiredLookAt;


    private bool nextLevelLocked = false;
    public Material playerMaterial;

    public Text currencyText;

    private int[] costs = {0,150,150,150
                           ,300,300,300,300,
                            500,500,500,500,
                            1000,1000,1000,1000};

    public void Start()
    {
       

        ChangePlayerSkin(GameManagers.Instance.currentSkinIndex);
        currencyText.text = "Çilekler :" + GameManagers.Instance.currency.ToString();
        cameraTransform = Camera.main.transform;
       
        Sprite[] thumnails = Resources.LoadAll<Sprite>("Levels");
        foreach(Sprite thumnail in thumnails)
        {
            GameObject container = Instantiate(lvlButtonPrefab) as GameObject; //her oluşan level butonu için buton fabrikten bir button oluşur 
            container.GetComponent<Image>().sprite = thumnail; // butonlara Levels klasöründen import edilen image png ler atanır.
            container.transform.SetParent(lvlButtonContainer.transform,false);
             LevelData level = new LevelData(thumnail.name);
            container.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = (level.BestTime != 0.0f) ? level.BestTime.ToString("f") : "";

            container.transform.GetChild(1).GetComponent<Image>().enabled = nextLevelLocked;
            container.GetComponent<Button>().interactable = !nextLevelLocked;
            if(level.BestTime == 0.0f)
            {
                nextLevelLocked = true;
            }

            string sceneName = thumnail.name;
            container.GetComponent<Button>().onClick.AddListener(() => LoadLevel(sceneName));  //Container İçindeki buttonların tıklama görevi(sahne ismine göre yükle)
        }
        int textureIndex = 0;
        Sprite[] textures = Resources.LoadAll<Sprite>("Player");
        foreach (Sprite texture in textures)
        {
            GameObject container = Instantiate(shopButtonPrefab) as GameObject; //her oluşan level butonu için buton fabrikten bir button oluşur 
            container.GetComponent<Image>().sprite = texture; // butonlara Levels klasöründen import edilen image png ler atanır.
            container.transform.SetParent(shopButtonContainer.transform, false);

            int index = textureIndex;
            container.GetComponent<Button>().onClick.AddListener(() => ChangePlayerSkin(index));  //Container İçindeki buttonların tıklama görevi(sahne ismine göre yükle)
            container.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = costs[index].ToString();
            if ((GameManagers.Instance.skinAvailability & 1 << index) == 1 << index)
            {
                container.transform.GetChild(0).gameObject.SetActive(false);
            }
               
            textureIndex++;

        }
    }
    public void Update()
    {
        if(cameraDesiredLookAt != null)
        {
            cameraTransform.rotation = Quaternion.Slerp(cameraTransform.rotation, cameraDesiredLookAt.rotation, 3 * Time.deltaTime);
        }
    }

    private void LoadLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    public void LookAtMenu(Transform menuTransform)
    {
        cameraDesiredLookAt = menuTransform;
    }


    
    private void ChangePlayerSkin(int index)
    {
        if((GameManagers.Instance.skinAvailability & 1 << index ) == 1 << index)
        {
            float x = (index % 4) * 0.25f;
            float y = ((int)index / 4) * 0.25f;

            if (y == 0.0f)          ///Texture okunurken unity alt sıradan başlayıp terse doğru okuduğı için
                y = 0.75f;          // Texture un y konumundan terse işelm yaptırıp doğru sıralamaya göre texture ataması yapıyoruz.
            else if (y == 0.25f)
                y = 0.5f;
            else if (y == 0.50f)
                y = 0.25f;
            else if (y == 0.75f)
                y = 0.0f;
            playerMaterial.SetTextureOffset("_MainTex", new Vector2(x, y));

            GameManagers.Instance.currentSkinIndex = index;
            GameManagers.Instance.Save();
        }
        else
        {
            //Kullanıcının skini yoksa aldırıyoruz :)

            int cost = costs[index];
            Debug.Log(cost.ToString());

            if(GameManagers.Instance.currency >= cost)
            {
                GameManagers.Instance.currency -= cost;
                GameManagers.Instance.skinAvailability += 1 << index;
                GameManagers.Instance.Save();
                currencyText.text = "Çilekler :" + GameManagers.Instance.currency.ToString();
                shopButtonContainer.transform.GetChild(index).GetChild(0).gameObject.SetActive(false);
                ChangePlayerSkin(index);            }
        }


      
    }

   
}
