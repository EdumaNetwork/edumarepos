﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeCamera : MonoBehaviour {

    public Transform lookAt;
    public VirtualJoystick cameraJoystick;
    private float distance = 10.0f;
    private float currentx = 0.0f;
    private float currenty = 0.0f;
    private float sensivityX = 3.0f;
    private float sensivityY = 1.0f;



	
	// Update is called once per frame
	void Update () {

        currentx += cameraJoystick.InputDirection.x * sensivityX;
        currenty += cameraJoystick.InputDirection.z * sensivityY;
	}

     private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currenty, currentx, 0);
        transform.position = lookAt.position + rotation * dir;
        transform.LookAt(lookAt);
    }
}
