﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor : MonoBehaviour {


    public float moveSpeed = 5.0f;
    public float drag = 0.5f;
    public float terminalRotationSpeed = 25.0f;
    private int count;
    private int pickCount = 0;
    public VirtualJoystick moveJoystick;

    private Rigidbody controller;
    private Transform camTransform;
    // Use this for initialization
    void Start () {
        var gos = GameObject.FindGameObjectsWithTag("pick Up");
        pickCount = gos.Length;
        controller = GetComponent<Rigidbody>();
        controller.maxAngularVelocity = terminalRotationSpeed;
        controller.drag = drag;
        count = 0;
        camTransform = Camera.main.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {


        Vector3 dir = Vector3.zero;
        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if(dir.magnitude > 1)
           dir.Normalize();

        if(moveJoystick.InputDirection != Vector3.zero)
        {
            dir = moveJoystick.InputDirection;
        }

            Vector3 rotatedDir = camTransform.TransformDirection(dir);
            rotatedDir = new Vector3(rotatedDir.x, 0.0f, rotatedDir.z);

            rotatedDir = rotatedDir.normalized * dir.magnitude;
            controller.AddForce(dir * moveSpeed);
        
	}
    void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.CompareTag("pick Up")) //pickUp Tag ine sahip küpleri eşliyor
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            if(count >= pickCount)
            {
                LevelManager.Instance.Victory();
            }
            
        }
    }

}
