﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagers : MonoBehaviour {


    private static GameManagers instance;
    public static GameManagers Instance { get { return instance; } }

    public int currentSkinIndex = 0;
    public int currency = 0;
    public int skinAvailability = 1;
    // Use this for initialization
    private void Awake()
    {

        instance = this;
        DontDestroyOnLoad(gameObject);
        if (PlayerPrefs.HasKey("CurrentSkin"))
        {

            PlayerPrefs.GetInt("CurrentSkin");
            PlayerPrefs.GetInt("Currency");

            skinAvailability = PlayerPrefs.GetInt("SkinAvailability");
        }
        else
        {
            Save();
        }

    }

    // Update is called once per frame
    public void Save()
    {
        PlayerPrefs.SetInt("CurrentSkin", currentSkinIndex);
        PlayerPrefs.SetInt("Currency", currency);
        PlayerPrefs.SetInt("SkinAvailability", skinAvailability);
    }
}
